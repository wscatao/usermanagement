# Projeto processo seletivo 2SOW.

## Instruções para iniciar o projeto em sua máquina.

Primeiro faça o clone deste repositório para sua máquina e acesse a pasta do projeto. Rode o comando ```npm install``` / ```npm i``` e instale as dependencias necessárias para iniciar.

Após instalado basta rodar o comando ```npm start``` para iniciar a aplicação. Além disso é preciso iniciar o servidor local do json-server conforme especificado nos requisitos: ```json-server --watch db.json --delay 2000 --port 5000```.

Caso queira executar os testes unitários basta digitar o comando ```npm test```, os testes estão na pasta ```src/app/__tests__```.

## Organização do projeto

O projeto foi dividido em uma pasta para components (/topbar) e outra pasta para as views (/pages), funções de requisição de Api e Validações podem ser encontradas em /utils.

